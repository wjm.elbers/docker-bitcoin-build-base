#!/usr/bin/env bash

if [ -z "${GITHUB_URL+x}" ]; then
    echo "GITHUB_ORG is not set"
    exit 1
fi

if [ -z "${VERSION+x}" ]; then
    echo "VERSION is not set"
    exit 1
fi

if [ -z "${NAME+x}" ]; then
    echo "NAME is not set"
    exit 1
fi

mkdir -p /src
cd /src

IS_TARBALL=$(echo "${GITHUB_URL}" | grep ".tar.gz")
if [ "${IS_TARBALL}" == "" ]; then
    echo "Cloning ${GITHUB_URL} and checking out tags/v${VERSION} into /src/${NAME}-${VERSION}"
    git clone ${GITHUB_URL} ${NAME}-${VERSION}
    cd ${NAME}
    git checkout tags/v${VERSION}
else
    echo "Downloading and building ${GITHUB_URL}"
    curl -L ${GITHUB_URL} | tar -zx
    pwd **=&& ls -l /src
fi

cd "${NAME}-${VERSION}"
pwd && ls -l
ls -l src

NUM_CORES=$(nproc)
HALF_NUM_CORES=$(($(nproc) / 2))
if [ "${HALF_NUM_CORES}" == "0" ]; then
    HALF_NUM_CORES=1
fi
echo "Found ${NUM_CORES} cores, compiling with ${HALF_NUM_CORES} cores"

#mkdir -p /release

if [ -f "./src/makefile.unix" ]; then
    apt-get update
    apt-get install -y libminiupnpc-dev

    cd src
    make -j"${HALF_NUM_CORES}" -f makefile.unix

#    bin=$(echo $NAME | tr '[:upper:]' '[:lower:]')
#    mv /src/${NAME}-${VERSION}/src/${bin}d /release/${bin}d
else
    ./autogen.sh
    EXTRA_ARGS=""
    if [ "${DISABLE_WALLET}" == "1" ]; then
        EXTRA_ARGS="${EXTRA_ARGS} --disable-wallet"
    fi
    ./configure --enable-cxx --disable-shared --with-pic ${EXTRA_ARGS}
    make -j"${HALF_NUM_CORES}"

#    pwd
#    ls -l /src/${NAME}-${VERSION}/src/
#    mv /src/${NAME}-${VERSION}/src/${NAME}-cli /release/${NAME}-cli
#    mv /src/${NAME}-${VERSION}/src/${NAME}-tx /release/${NAME}-tx
#    mv /src/${NAME}-${VERSION}/src/${NAME}d /release/${NAME}d
fi


